Reports plugin enablement times exposed by the `com.atlassian.plugin.Plugin.EnabledMetricsSource` interface, thus it is only compatible with `atlassian-plugins(-core)` >= `3.2.0`.
The times are exposed via `/plugins/servlet/plugin-status`.

You can use the `client.py` script to get a different formatting of the data exposed by Noddy's REST endpoint.

    python -c"`curl -sL 'https://maven.atlassian.com/service/local/repositories/atlassian-public/content/com/atlassian/plugins/atlassian-plugin-status-plugin/v1.0.0/atlassian-plugin-status-plugin-1.0.0-client.py'`" --help

This way you can source the script from the remote, or simply clone this repository and run `mvn package -DskipTests` and then

    python atlassian-plugin-status-plugin/target/classes/client.py --help

You can specify the target server via `--server`, which expects the application's base URL

    python atlassian-plugin-status-plugin/target/classes/client.py --server https://pug.jira.com/wiki

The script accepts a filter of plugin keys either from stdin or with the `--filter option`. The provided keys only have to be partial matches

    echo 'com.atlassian.streams.actions\ncom.atlassian.streams.core'|python atlassian-plugin-status-plugin/target/classes/client.py
    $> plugin com.atlassian.streams.actions                                                    took 07757 ms to enable starting at 2014-07-08 14:26:42
    $> plugin com.atlassian.streams.core                                                       took 07298 ms to enable starting at 2014-07-08 14:26:42

This filter can be inversed via the `--inverse` option, turning the provided list from a whitelist into a blacklist.
Together with the filter, you can use this option to segment the plugins into the two instanton phases.
For example, the [early_startup_plugin_keys.py](https://stash.atlassian.com/projects/CONF/repos/confluence/browse/scripts/instanton/early_startup_plugin_keys.py) script extracts a list of phase 1 plugin key fragments for Confluence.

You can `--format` the output. For example if you'd want to get the total plugin enablement duration, you could do something like

    python atlassian-plugin-status-plugin/target/classes/client.py --format '%(duration)d'|awk '{s+=$1} END {print s}'
    $> 597140

Note that the duration times are in milliseconds.