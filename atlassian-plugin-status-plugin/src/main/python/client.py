MIN_PYTHON_VERSION = (2, 6)

import sys

if sys.version_info < MIN_PYTHON_VERSION:
    sys.exit('Python %s.%s was used, but Python >= %s.%s is required.' % (
        sys.version_info[0], sys.version_info[1], MIN_PYTHON_VERSION[0], MIN_PYTHON_VERSION[1]))

import optparse
import json
from datetime import datetime as date
import fileinput
import httplib2
import os
from functools import reduce

DATE_FORMAT = '%b %d, %Y %I:%M:%S %p'
GROUP_ID = '${project.groupId}'
ARTIFACT_ID = '${project.artifactId}'
VERSION = '${project.version}'
PLUGIN_KEY = GROUP_ID + "." + ARTIFACT_ID
REST_EP = '/plugins/servlet/plugin-status'
RELEASE = "https://maven.atlassian.com/service/local/artifact/maven/redirect?r=atlassian-public&g={0}&a={1}&v={2}&e=jar".format(
    GROUP_ID, ARTIFACT_ID, VERSION)


def install(server):
    sys.exit('atlassian-plugin-status-plugin is not installed on {0}, please install it from {1}'.format(server, RELEASE))


def get_plugin_info(server):
    url = server + REST_EP

    try:
        response, content = httplib2.Http().request(url)
    except AttributeError, e:
        if "'NoneType' object has no attribute 'makefile'" == str(e):
            sys.exit('Socket is not open for {0}'.format(url))
        raise

    if response.status == 404:
        install(server)
    elif response.status != 200:
        sys.exit('Unexpected response code {0} from atlassian-plugin-status-plugin at {1}'.format(response.status, url))

    try:
        json_content = json.loads(content.decode())
    except ValueError:
        install(server)
    return [{'key': plugin['key'], 'duration': int(plugin['enableDuration']),
             'date': date.strptime(plugin['dateEnabling'], DATE_FORMAT)} for plugin in
            json_content['enabledPlugins']]


def get_plugin_key_predicate(filter_name, blacklist, include_zero):
    plugin_keys = []
    if (filter_name != '-'):
        with open(os.path.expanduser(filter_name), 'r') as filter:
            for line in filter:
                plugin_keys.append(line.rstrip())
    elif not sys.stdin.isatty():
        for line in sys.stdin.readlines():
            plugin_keys.append(line.rstrip())

    filter_predicate = lambda plugin: True
    if len(plugin_keys) > 0:
        count = lambda plugin: len([plugin_key for plugin_key in plugin_keys if plugin_key in plugin['key']])
        filter_predicate = lambda plugin: count(plugin) > 0
        if blacklist:
            filter_predicate = lambda plugin: count(plugin) == 0
    not_predicate = lambda plugin: plugin['key'] != PLUGIN_KEY
    include_zero_predicate = lambda plugin: True
    if not include_zero:
        include_zero_predicate = lambda plugin: plugin['duration'] != 0
    return lambda plugin: filter_predicate(plugin) and not_predicate(plugin) and include_zero_predicate(plugin)

if __name__ == "__main__":

    parser = optparse.OptionParser(description='''Format and scope the output of {0} REST endpoint'''.format(REST_EP))
    parser.add_option('--server',
                      help='''The base URL of the application atlassian-plugin-status-plugin is installed at, default is \033[94m\033[1m%default\033[0m\033[0m''',
                      default='http://localhost:8080/confluence')
    parser.add_option('--order',
                      help='''The output order, default is \033[94m\033[1m%default\033[0m\033[0m. Order by plugin \033[94m\033[1mkey\033[0m\033[0m, enable start \033[94m\033[1mdate\033[0m\033[0m or enable \033[94m\033[1mduration\033[0m\033[0m. Can be sorted lexicographically by appending either \033[94m\033[1masc\033[0m\033[0mending or \033[94m\033[1mdesc\033[0m\033[0mending.''',
                      default='duration desc')
    parser.add_option('--format',
                      help='''The output format, default is \033[94mplugin %(key)-80s taking %(duration)05d ms to enable, started at %(date)s\033[0m. E.g. use \033[94m\033[1m%(duration)d\033[0m\033[0m and pipe (|) the output through \033[94m\033[1mawk '{s+=$1} END {print s}'\033[0m\033[0m to get the total.''',
                      default='plugin \033[94m%(key)-80s\033[0m took \033[94m%(duration)05d\033[0m ms to enable starting at \033[94m%(date)s\033[0m')
    parser.add_option('--filter',
                      help='Plugin key whitelist. A file (or stdin) containing plugin keys (one per line) to be matched. No exact match is required. If a tested plugin key is composed of one of the given keys it is white listed, thus it will be part of the output.',
                      default='-')
    parser.add_option('--inverse', help='Turn the whitelist into a blacklist', action='store_true', default=False)
    parser.add_option('--zero', help='By default, zero durations are excluded. Plugins reported with zero duration are either OSGi bundles or virtual plugins, or simply have no Spring DM managed lifecycle.', action='store_true', default=False)
    config, args = parser.parse_args()

    filter = get_plugin_key_predicate(config.filter, config.inverse, config.zero)
    plugins = [plugin for plugin in get_plugin_info(config.server) if filter(plugin)]

    plugins = sorted(plugins, key=lambda plugin: plugin[config.order.split()[0]])
    if len(config.order.split()) > 1 and config.order.split()[1] == 'desc':
        plugins = reversed(plugins)

    for plugin in plugins:
        print(config.format % plugin)