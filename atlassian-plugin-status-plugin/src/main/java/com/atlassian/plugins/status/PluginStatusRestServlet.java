package com.atlassian.plugins.status;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PluginStatusRestServlet extends HttpServlet
{
    private final Logger log = LoggerFactory.getLogger(PluginStatusRestServlet.class);

    private final SystemStateStore systemStateStore;

    private final EnabledPluginsStore enabledPluginsStore;

    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public PluginStatusRestServlet(final SystemStateStore systemStateStore, final EnabledPluginsStore enabledPluginsStore)
    {
        log.debug("<init>");

        this.systemStateStore = systemStateStore;
        this.enabledPluginsStore = enabledPluginsStore;
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.debug("doGet");

        PluginStatus pluginStatus = new PluginStatus(systemStateStore.getSystemState(), enabledPluginsStore.getEnabledPlugins());

        resp.setContentType("text/json");
        resp.getWriter().write(gson.toJson(pluginStatus));
    }

    @SuppressWarnings ("UnusedDeclaration")
    private class PluginStatus
    {
        private SystemState systemState;

        private List<EnabledPlugin> enabledPlugins;

        private PluginStatus(final SystemState systemState, final List<EnabledPlugin> enabledPlugins)
        {
            this.systemState = systemState;
            this.enabledPlugins = enabledPlugins;
        }
    }
}
