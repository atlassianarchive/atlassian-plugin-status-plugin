package com.atlassian.plugins.status;

public enum SystemState
{
    STARTING,
    WARM,
    TENANTED
}
