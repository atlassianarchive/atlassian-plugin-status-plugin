package com.atlassian.plugins.status;

import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginFrameworkDelayedEvent;
import com.atlassian.plugin.event.events.PluginFrameworkStartedEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings ("UnusedDeclaration")
public class SystemStateStore
{
    private final Logger log = LoggerFactory.getLogger(SystemStateStore.class);

    private final PluginEventManager pluginEventManager;

    private SystemState systemState = SystemState.STARTING;

    public SystemStateStore(final PluginEventManager pluginEventManager)
    {
        log.debug("<init>");

        this.pluginEventManager = pluginEventManager;

        pluginEventManager.register(this);
    }

    @PluginEventListener
    public void onPluginFrameworkDelayed(final PluginFrameworkDelayedEvent event)
    {
        log.debug("onPluginFrameworkDelayed");

        systemState = SystemState.WARM;
    }

    @PluginEventListener
    public void onPluginFrameworkStarted(final PluginFrameworkStartedEvent event)
    {
        log.debug("onPluginFrameworkStarted");

        systemState = SystemState.TENANTED;
    }

    // todo amc: deregister from events

    public SystemState getSystemState()
    {
        return systemState;
    }
}
