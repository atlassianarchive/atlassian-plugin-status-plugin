package com.atlassian.plugins.status;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnabledPluginsStore
{
    private final Logger log = LoggerFactory.getLogger(EnabledPluginsStore.class);

    private final PluginAccessor pluginAccessor;

    public EnabledPluginsStore(final PluginAccessor pluginAccessor)
    {
        log.debug("<init>");
        this.pluginAccessor = pluginAccessor;
    }

    public List<EnabledPlugin> getEnabledPlugins()
    {
        log.debug("getEnabledPlugins");

        List<EnabledPlugin> enabledPlugins = new ArrayList<EnabledPlugin>();

        for (Plugin plugin : pluginAccessor.getEnabledPlugins())
        {
            enabledPlugins.add(new EnabledPlugin(plugin));
        }
        Collections.sort(enabledPlugins);

        return enabledPlugins;
    }
}
