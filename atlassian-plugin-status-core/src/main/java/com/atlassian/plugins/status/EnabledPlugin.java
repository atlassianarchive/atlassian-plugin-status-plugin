package com.atlassian.plugins.status;

import java.util.Date;

import com.atlassian.plugin.Plugin;

import org.apache.commons.lang.ObjectUtils;

@SuppressWarnings ("UnusedDeclaration")
public class EnabledPlugin implements Comparable<EnabledPlugin>
{
    private Date dateEnabling;
    private Date dateEnabled;
    private long enableDuration;
    private String name;
    private String key;

    public EnabledPlugin(final Plugin plugin)
    {
        dateEnabling = Plugin.EnabledMetricsSource.Default.getDateEnabling(plugin);
        dateEnabled = Plugin.EnabledMetricsSource.Default.getDateEnabled(plugin);
        name = plugin.getName();
        key = plugin.getKey();

        if (dateEnabled != null && dateEnabling != null)
        {
            enableDuration = dateEnabled.getTime() - dateEnabling.getTime();
        }
    }

    @Override
    public int compareTo(final EnabledPlugin o)
    {
        int ret = ObjectUtils.compare(dateEnabling, o.dateEnabling);
        if (ret == 0)
        {
            ret = ObjectUtils.compare(name, o.name);
        }
        return ret;
    }
}
